package hackathon.gotham.chathead;

import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.IBinder;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import java.io.IOException;

public class ChatHeadService extends Service {

    public static final String NUMBER = "number";

    private WindowManager windowManager;
    private ImageView chatHead;

    @Override
    public IBinder onBind(Intent intent) {
        // Not used
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        createChatHead(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final int result = super.onStartCommand(intent, flags, startId);
        createChatHead(intent);
        return result;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeChatHead();
    }

    public void createChatHead(Intent intent) {
        if (intent == null) {
            removeChatHead();
            return;
        }

        final String threadId = intent.getStringExtra(NUMBER);

        if (threadId == null) {
            return;
        }

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);


        removeChatHead();
        chatHead = new ImageView(this);
        chatHead.setImageResource(R.mipmap.ic_launcher);
        String number = intent.getStringExtra(NUMBER);

        Uri u = getPhotoUri(getContactDisplayNameByNumber(number));
        if (u != null) {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), u);
                chatHead.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                200,
                200,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.START;
        params.x = 0;
        params.y = 0;

        chatHead.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            private long clickStart = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        clickStart = System.currentTimeMillis();
                        return true;
                    case MotionEvent.ACTION_UP:
                        if (System.currentTimeMillis() < clickStart + 1000) {
                            fakeClick(v);
                        }
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);
                        windowManager.updateViewLayout(chatHead, params);
                        return true;
                }
                return false;
            }

            private void fakeClick(View v) {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + threadId));
                sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(sendIntent);
                removeChatHead();
            }
        });

        chatHead.setClickable(true);
        windowManager.addView(chatHead, params);
    }

    private void removeChatHead() {
        if (chatHead != null) {
            windowManager.removeView(chatHead);
            chatHead = null;
        }
    }

    public String getContactDisplayNameByNumber(String number) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        String id = null;

        ContentResolver contentResolver = getContentResolver();
        Cursor contactLookup = contentResolver.query(uri,
                new String[]{BaseColumns._ID, ContactsContract.PhoneLookup.DISPLAY_NAME},
                null,
                null,
                null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                id = contactLookup.getString(contactLookup.getColumnIndex(BaseColumns._ID));
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }

        return id;
    }

    public Uri getPhotoUri(String id) {
        if (id == null) {
            return null;
        }

        Cursor cur = null;
        try {
            cur = getContentResolver().query(
                    ContactsContract.Data.CONTENT_URI,
                    null,
                    ContactsContract.Data.CONTACT_ID + "=" + id + " AND "
                            + ContactsContract.Data.MIMETYPE + "='"
                            + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE + "'",
                    null,
                    null);
            if (cur != null) {
                if (!cur.moveToFirst()) {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long
                .parseLong(id));
        return Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
    }

}
